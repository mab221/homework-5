# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

    = ((λp.(pz))(λq.(w(λw.((((wq)z)p))))))

2. λp.pq λp.qp

    = (λp.((pq)(λp.(qp))))

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q

    s is bound and q is bound 
    z is free

2. (λs. s z) λq. w' λw. w q z s

    first paranthesis: 
        s is bound 
        z is free

    second opening:
        q is bound w is bound
        s is free w' is free z is free  

3. (λs.s) (λq.qs)

    first parenthesis:
        s is bound
    second parenthesis:
        q is bound
        s is free


4. λz. (((λs.sq') (λq.qz)) λz. (z z))

    z is bound s is bound  q is bound 
    q' is free

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
 
    [a->s']s'a' = aa'


2. (λz.z) (λz.z z) (λz.z q)

    [q->z']z'q' = qq'

3. (λs.λq.s q q) (λa.a) b

    [bb->a]a = bb

4. (λs.λq.s q q) (λq.q) q

    [q->z']z'q' = q''q''

5. ((λs.s s) (λq.q)) (λq.q)

    [(λq.q->q')q'] = λq.q


## Question 4

1. Write the truth table for the or operator below.
 OR Truth Table
  -----|----- 
   T T | T
   T F | T
   F T | T
   F F | F

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

True True:
 (λp.λq.p p q) (λa.λb.a) (λa.λb.a)
        ([(λa.λb.a) -> p]λq.p p q)(λa.λb.a) = (λq.(λa.λb.a)(λa.λb.a)q)(λa.λb.a)
        [(λa.λb.a) -> q](λq.(λa.λb.a)(λa.λb.a)q) = (λa.λb.a)(λa.λb.a)(λa.λb.a)
        ([(λa.λb.a) -> a]λb.a)(λa.λb.a) = (λb.(λa.λb.a))(λa.λb.a)
        [(λa.λb.a) -> b](λa.λb.a) = (λa.λb.a) = T
    TF
        (λp.λq.p p q) T F
        ([T -> p]λq.p p q)F = (λq.T T q)F
        [F -> q]T T q = T T F = T
    FT
        (λp.λq.p p q) F T
        ([F -> p]λq.p p q) T = (λq.F F q) T
        [T -> q]F F q = F F T = T
    FF
        (λp.λq.p p q) F F
        ([F -> p]λq.p p q) F = (λq.F F q) F
        [F -> q]F F q = F F F = F




## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

(λp.p F T) λa.λb.a = F || (λp.p F T) λa.λb.b = T

an example of the if statement lambda expression is λp.λt.λf.p T F therefore it returns true or false depending on the 
validity of the data being processed. This is similar to the structure of the not where it takes boolean conditions
to reach a decision. 



## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.